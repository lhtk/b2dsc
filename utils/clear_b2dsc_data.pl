#!/usr/bin/env perl
# by lhtk : lhtk80t7@gmail.com
# 
# 自动化流程，为 B2DSC 删除数据， Faidx 同时删除。
# 
use strict;
use warnings;
# use JSON;
use Cwd 'abs_path';
use Getopt::Long;

my $b2dsc_dir;
my $dbname;
my $help;

GetOptions(
    'help|h'    => \$help,
    'b2dsc_dir|b2dsc|b=s'   => \$b2dsc_dir,
    'dbname|n=s'  => \$dbname,
) or die "Error in command line arguments\n";

my %dirs;
check_args();

# ===============================================================
for my $cdir (qw/b2dsc faidx/) {
    my $fpath_conf = $dirs{conf}{$cdir} . '/' . "${dbname}.json";
    if (-e $fpath_conf) {
        print "发现配置文件  $fpath_conf，请确认是否删除（[yes, no]：\n";
        system "rm -iv $fpath_conf";
    } else {
        print "未发现配置文件  $fpath_conf\n";
    }
}

for my $dn (qw/bdb Ns fasta/) {
    if (-e $dirs{$dn}) {
        print "发现文件夹 $dirs{$dn}，是否删除（[yes, no]）：\n";
        print "请选择：";
        my $choice = <STDIN>;
        if ($choice =~ /^y(es)?/i) {
            system "rm -rv $dirs{$dn}";
        }
    } else {
        print "未发现文件夹 $dirs{$dn}\n";
    }
}

print "\n已按您的选择处理。\n";

# ===============================================================
# subroutines
# ===============================================================

sub check_args {
    usage() if ($help);

    unless (defined $dbname and $dbname =~ /^[\w\-]+$/) {
        die "请设定正确的数据库名称！（-n）\n";
    }

    unless (defined $b2dsc_dir) {
        # $b2dsc_dir = '/home/lhtk/sites/mcgb';
        die "必须指定 B2DSC 安装目录，类似: /home/lhtk/git/b2dsc\n";
    } else {
        if ($b2dsc_dir =~ /^~/) { # e.g. ~/git/b2dsc
            $b2dsc_dir = glob($b2dsc_dir);
        } else {
            $b2dsc_dir = abs_path($b2dsc_dir);
        }
    }
    die "$b2dsc_dir: 该文件夹不存在！\n" unless -d $b2dsc_dir;

    if (not -o $b2dsc_dir) {
        die "当前用户并非文件夹 --> $b2dsc_dir <-- 的所有者！\n请切换至正确账户!\n";
    }

    %dirs = (
        bdb => join('/', $b2dsc_dir, 'data/blastdb', $dbname),
        Ns => join('/', $b2dsc_dir, 'data/Ns', $dbname),
        fasta => join('/', $b2dsc_dir, 'data/fasta', $dbname),
        conf => {
            b2dsc => join('/', $b2dsc_dir, 'conf/b2dsc/species'),
            faidx => join('/', $b2dsc_dir, 'conf/faidx'),
        },
    );
}

sub usage {
    print <<USAGE;

为 B2DSC 删除数据， Faidx 相应内容同时删除。
将删除指定的 B2DSC 服务所在文件夹下的如下文件或文件夹：

    文件
        \$b2dsc_dir/conf/b2dsc/species/Your_database_name.json
        \$b2dsc_dir/conf/faidx/Your_database_name.json
    文件夹
        \$b2dsc_dir/data/blastdb/Your_database_name/
        \$b2dsc_dir/data/Ns/Your_database_name/
        \$b2dsc_dir/data/fasta/Your_database_name/

不在 B2DSC 服务所在文件夹的不会删除。

用法示例：
    
    $0 -n wheat_1A -b ~/git/b2dsc

选项：

    --b2dsc_dir, --b2dsc, -b
        B2DSC 或者 MCGB 网站所在目录。
        例如: /home/lhtk/sites/mcgb 或 /home/lhtk/git/b2dsc

        注意：必须使用这一目录所有者账号，例如 lhtk 用户。

    --dbname, -n
        数据库名称，请仅使用以下字符：
        1. 英文字母 a-zA-Z
        2. 阿拉伯数字 0-9
        3. 下划线 _
        4. 短横线 -

        例如: Aegilops_speltoides

    --help, -h
        打印此帮助。
        可选。
        flag
USAGE
    exit 0;
}
