#!/bin/bash

fadir='../../fasta/example/'

if [ ! -d log ]; then
    echo '创建 log 文件夹'
    mkdir log
fi

# 例子里只有 1 条序列
subGs=(A)
for hgrp in `seq 1 1`; do
    for subG in ${subGs[@]}; do
        cname=$hgrp$subG
        echo $cname
        makeblastdb -in $fadir/chr$cname.fa -dbtype nucl -title "example chr$cname" -parse_seqids -out $cname -logfile ./log/$cname.log
    done
done

echo Done!
