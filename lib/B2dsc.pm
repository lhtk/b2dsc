package B2dsc;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;

  # Load configuration from hash returned by "my_app.conf"
  my $config = $self->plugin('Config');

  # Documentation browser under "/perldoc"
  # PODRenderer 在新版框架中已经被抛弃了, 2020.8.25
  # $self->plugin('PODRenderer') if $config->{perldoc};

  # Router
  my $r = $self->routes;

  # Normal route to controller
  # $r->get('/')->to('example#welcome');
  $r->get('/')->to('home#index');

  # B2DSC
  $r->get('/b2dsc')->to('b2dsc#index');
  $r->post('/b2dsc/blastn')->to('b2dsc#blastn');
  $r->post('/b2dsc/filter_bls')->to('b2dsc#filter_bls');
  $r->get('/b2dsc/job_status')->to('b2dsc#job_status');
  # $r->post('/b2dsc/draw_pics')->to('b2dsc#draw_pics');
  $r->get('/b2dsc/known_probes')->to('b2dsc#known_probes');
  $r->get('/b2dsc/get_seq')->to('b2dsc#get_seq');
  $r->get('/b2dsc/handle_ajax')->to('b2dsc#handle_ajax');
  $r->get('/b2dsc/parameters')->to('b2dsc#parameters');
  $r->get('/b2dsc/about')->to('b2dsc#about');
  $r->get('/b2dsc/help')->to('b2dsc#help');
  $r->get('/b2dsc/contact')->to('b2dsc#contact');
  $r->get('/b2dsc/count2tsv')->to('b2dsc#count2tsv');
  $r->get('/b2dsc/update_log')->to('b2dsc#update_log');
  
  # Faidx
  $r->get('/faidx')->to('faidx#index');
  $r->get('/faidx/handle_ajax')->to('faidx#handle_ajax');
}

1;
