$(document).ready(function () {
    // sample seq
    $('#sample-seq').click(function (event) {
        event.preventDefault(); // 避免改掉 location，发生跳转
        $('#qSeq').val('>pSc119.2_1\nCCGTTTTGTGGACTATTACTCACCGCTTTGGGGTCCCATAGCTAT');
    });

    function on_click_selector() {
        $('.selector').click(function () {
            var v = $(this).val();
            if ($(this).is(':checked')) {
                $('.' + v).prop('checked', true);
            } else {
                $('.' + v).prop('checked', false);
            }
        });
    }
    on_click_selector(); // 执行一次

    // blastdbs
    $('#species').change(function () {
        $('#chromosomes').html(''); // 2019.7.14
        var species = $(this).val();
        if (species) {
            $.getJSON('/b2dsc/handle_ajax', {
                category: 'blastdb', species: species
            }, function (chromosome) {
                var subGs = Object.keys(chromosome), txt = '';
                subGs.forEach(subG => {
                    txt += '<dt class="' + species + '">' +
                        '<input class="selector" id="' + species + '-g' +
                        subG + '" name="check_' + subG +
                        '" title="select/unselect ' + subG +
                        ' genome" type="checkbox" value="' + species + '-' + subG + '_genome">' + '<label for="' +
                        species + '-g' + subG + '"> ' + subG + ' </label></dt>';
                    txt += '<dd class="' + species + '">';
                    // console.log(chromosome); // debug
                    Object.keys(chromosome[subG])
                        .sort((a, b) => ((+a) - (+b))).forEach((hgrp, idx) => {
                            txt += '<input class="' + species + '-' + subG +
                                '_genome" id="chr' + hgrp + subG + '" name="chrs[]" type="checkbox" value="' + hgrp + subG + '">';
                            txt += '<label for="chr' + hgrp + subG + '"> ' + hgrp + ' </label>';
                            if ((idx + 1) % 10 === 0) txt += '<br>';
                        });
                    txt += '</dd>';
                });
                $('#chromosomes').html(txt);

                // $('.selector').click(function () {
                //     var v = $(this).val();
                //     if ($(this).is(':checked')) {
                //         $('.' + v).prop('checked', true);
                //     } else {
                //         $('.' + v).prop('checked', false);
                //     }
                // });
                on_click_selector();
            });
        } else {
            $('#chromosomes').html(
                '<b style="color: red">Please select species.</b>'
            );
        }
    });

    // Parameters for similar sequences
    $('#filter_seq').hide();
    $('#means_of_disable').hide();
    $('#bl2f').click(function () {
        // $('#filter_seq').toggle();
        if ($('#bl2f').is(':checked')) {
            $('#filter_seq').show("fast");
        } else {
            $('#filter_seq').hide("fast");
        }
    });
    $('#disable_SM').click(function () {
        if ($('#disable_SM').is(':checked')) {
            $('#means_of_disable').show("fast");
        } else {
            $('#means_of_disable').hide("fast");
        }
    });

    $('#alg-params').hide();
    $('.gt-or-char').css('color', '#fff');
    $('#toggle-alg-params').click(function (event) {
        event.preventDefault();
        $('#toggle-alg-params span.gt-or-char').html(
            $('#alg-params').css('display') == 'none' ? '–' : '+'
        );
        $('#alg-params').toggle("normal");
    });

    $('.spec-required').click(function () {
        return false;
    });

    // $('#opt-features').hide();
    // $('#toggle-opt-features').click(function (event) {
    //     event.preventDefault();
    //     $('#opt-features').toggle();
    // });
});

function checkForm(form, value) {
    var submitForm = true;

    // query sequence
    if (form.querySeq.value.match(/^\s*$/)) {
        alert("Please enter your query sequence.");
        submitForm = false;
        return;
    }
    if (!form.querySeq.value.match(/^[\x00-\x7F]+$/)) {
        alert("Query sequence: you entered strange characters! ASCII code only, please.");
        submitForm = false;
        return;
    }

    // chromosomes OK?
    var chrs = 0;
    var tmp_el = document.getElementsByTagName('input');
    for (var i = 0; i < tmp_el.length; i++) {
        if ((tmp_el[i].type == "checkbox") && (tmp_el[i].name == 'chrs[]') && (tmp_el[i].checked == true)) {
            chrs += 1;
        }
    }

    if (chrs == 0) {
        var msg1 = "None chromosome has been slected!";
        alert(msg1);
        submitForm = false;
        return;
    }

    // submit
    if (submitForm == true) {
        form.submit();
    }
}