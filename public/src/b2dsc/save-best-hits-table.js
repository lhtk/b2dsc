// 让用户可以直接保存最佳匹配表
$(document).ready(function () {
    var newLine = navigator.platform.match(/^Win/i) ? "\r\n" : "\n";
    $('#save-best-hits-table').click(function (event) {
        event.preventDefault();
        $.getJSON('/b2dsc/handle_ajax', {
            jid: $('#job-id').text(), category: 'done',
            keys: 'best_hits ofmt'
        }, function (done) {
            var headers = done.ofmt.replace(/^6\s+/, '').split(' ');
            var txt, ofmtHasQseqid = false;
            if (done.ofmt.indexOf('qseqid') != -1) ofmtHasQseqid = true;
            txt = ofmtHasQseqid ? headers.join("\t") : "qseqid\t" + headers.join("\t");
            txt += newLine;
            for (var sid in done.best_hits) {
                done.best_hits[sid].forEach(d => {
                    txt += sid + "\t" + d.join("\t") + newLine;
                });
            }
            var blob = new Blob([txt], { type: 'text/plain;charset=utf-8' });
            saveAs(blob, "tmp.txt");
        });
    });
});