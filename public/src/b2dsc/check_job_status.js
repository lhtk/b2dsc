function check_job_status() {
    // $.ajaxSetup({
    //     // Disable caching of AJAX responses
    //     cache: false
    // });
    $.getJSON('/b2dsc/handle_ajax', {
        jid: $('#job-id').text(), category: 'done', keys: 'status'
    }, function (json) {
        if (json.status === 'free') {
            window.location.reload();
        }
    });
}