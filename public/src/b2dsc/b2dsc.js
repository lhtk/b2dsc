function changeChromosomes(x) {
    var e = document.getElementById(x); // all species
    var species = e.value;
    var e_opts = e.options;
    for (var i = 0; i < e_opts.length; i++) {
        var elements = document.getElementsByClassName(e_opts[i].value);
        if (species == e_opts[i].value) {
            for (var j = 0; j < elements.length; j++) {
                elements[j].style.display = "block";
            }
        } else {
            for (var j = 0; j < elements.length; j++) {
                elements[j].style.display = "none";
            }
        }
    }

    // 全部反选
    clearAllByName('chrs[]');
    clearAllByClass('selector');
}

function checkForm(form, value) {
    var submitForm = true;

    // query sequence
    if (form.querySeq.value.match(/^\s*$/)) {
        alert("Please enter your query sequence.");
        submitForm = false;
        return;
    }

    // chromosomes OK?
    var chrs = 0;
    var tmp_el = document.getElementsByTagName('input');
    for (var i = 0; i < tmp_el.length; i++) {
        if ((tmp_el[i].type == "checkbox") && (tmp_el[i].name == 'chrs[]') && (tmp_el[i].checked == true)) {
            chrs = chrs + 1;
        }
    }

    if (chrs == 0) {
        var msg1 = "None chromosome has been slected!";
        alert(msg1);
        submitForm = false;
        return;
    }

    // submit
    if (submitForm == true) {
        form.submit();
    }
}

// 说明：Javascript 控制 CheckBox 的全选与取消全选
// 整理：http://www.CodeBit.cn

function checkAllByClass(className) {
    var el = document.getElementsByClassName(className);
    var len = el.length;
    for (var i = 0; i < len; i++) {
        if ((el[i].type == "checkbox")) {
            el[i].checked = true;
        }
    }
}
function clearAllByClass(className) {
    var el = document.getElementsByClassName(className);
    var len = el.length;
    for (var i = 0; i < len; i++) {
        if ((el[i].type == "checkbox")) {
            el[i].checked = false;
        }
    }
}
function clearAllByName(name) {
    var el = document.getElementsByTagName('input');
    var len = el.length;
    for (var i = 0; i < len; i++) {
        if ((el[i].type == "checkbox") && (el[i].name == name)) {
            el[i].checked = false;
        }
    }
}

// 20 s 刷新一次
function refresh_this_page() {
    var url = window.location.href;
    if (url.match(/refresh/)) {
        url = url.replace(/(&?)(refresh)=[^&]*(&|$)/, "$1$2=1$3");
    } else {
        url = url + "&refresh=1";
    }
    setTimeout(function () { window.location.href = url; }, 1000 * 2);
}

function stop_refresh_page() {
    var url = window.location.href;
    if (url.match(/refresh/)) {
        url = url.replace(/(&?)(refresh)=[^&]*(&|$)/, "$1$2=0$3");
    } else {
        url = url + "&refresh=0";
    }
    setTimeout(function () { window.location.href = url; }, 1000 * 1);
}