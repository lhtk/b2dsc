// Filter blast results
$(document).ready(function () {
    $('#id4filter').change(function () {
        var sid = $(this).val();
        // var jobUrl = '/TMP/' + $('#job-id').text();
        // $.getJSON(jobUrl + '/done.json', function (json) {
        $.getJSON('/b2dsc/handle_ajax', {
            jid: $('#job-id').text(), category: 'done', keys: 'min max'
        }, function (json) {
            var minpqs = json.min;
            var maxpqs = json.max;

            if (minpqs[sid]['pident'] != 'NA') {
                $('#found-hsp').show();
                $('#f_bls-submit').show();
                $('no-hsp-found').addClass('hidden');

                if (minpqs[sid]['pident'] > 85 || maxpqs[sid]['pident'] < 85) {
                    $('#pIdent').val(minpqs[sid]['pident']);
                } else {
                    $('#pIdent').val(85);
                }
                $('#pIdent').attr('title', 'min: ' + minpqs[sid]['pident'] + ' | max: ' + maxpqs[sid]['pident']);
                if (minpqs[sid]['qcovhsp'] > 80 || maxpqs[sid]['qcovhsp'] < 80) {
                    $('#qCover').val(minpqs[sid]['qcovhsp']);
                } else {
                    $('#qCover').val(80);
                }
                $('#qCover').attr('title', 'min: ' + minpqs[sid]['qcovhsp'] + ' | max: ' + maxpqs[sid]['qcovhsp']);
            } else {
                $('#found-hsp').hide();
                $('#f_bls-submit').hide();
                $('no-hsp-found').removeClass('hidden');
            }
        });
    });
});