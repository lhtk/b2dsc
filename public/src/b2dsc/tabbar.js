$(document).ready(function () {
    // 标签页效果
    $('.tabbar a').click(function (event) {
        event.preventDefault(); // 避免改掉 location，发生跳转
        $('.tabpage').addClass('hidden');
        var did = $(this).attr('href');
        $(did).removeClass('hidden');
        $('.tabbar a').removeClass('tabon');
        $(this).addClass('tabon');
    });
});