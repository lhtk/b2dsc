// referred: https://github.com/Gaohaoyang/gaohaoyang.github.io/blob/master/js/main.js
(function () {
  var backToTop = document.querySelector('.back-top')
  var backToTopA = document.querySelector('.back-top a')
  
  window.addEventListener('scroll', function () {
    // 页面顶部滚进去的距离
    var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop)

    if (scrollTop > 200) {
      backToTop.style.display = 'block'
    } else {
      backToTop.style.display = 'none'
    }
  })

  backToTopA.addEventListener('click', function (e) {
    e.preventDefault()
    window.scrollTo(0, 0)
  })
}());