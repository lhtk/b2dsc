$(document).ready(function () {
    var pathname = window.location.pathname;
    if (pathname != '/') {
        pathname = pathname.replace(/\/$/, '');
    }
    $('div#nav a').mouseover(function () {
        $(this).css("text-decoration", "underline");
    }).mouseout(function () {
        $(this).css("text-decoration", "none");
    });
    // $('div#nav a[href="' + pathname + '"]').css({ "color": "#FF5722" }); // tnac
    // $('div#nav a[href="' + pathname + '"]').css({ "background-color": "#F3F3F3", "color": "#FF5722" }); // b2dsc
    $('div#nav a[href="' + pathname + '"]').addClass('active');
});