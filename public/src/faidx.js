$(document).ready(function () {
    $('#submit').hide();
    $('#next-previous').hide();
    $('#select-seqs').hide();
    $('#species').change(function () {
        var s = $('#species').val();
        if (s) {
            $('#chrName').html('');
            $('#submit').show();
            $.getJSON('/faidx/handle_ajax', {
                species: s
            }, function (data) {
                Object.keys(data).sort().forEach(subG => {
                    Object.keys(data[subG]).sort((a,b) => ((+a) - (+b)))
                    .forEach(hgrp => {
                        var arr = data[subG][hgrp];
                        $('#chrName').append('<option value="' + hgrp + subG +
                        '" title="Homologous group: ' + hgrp +
                        '\nSub-genome: ' + subG + '\nSequence ID: ' + arr[1] + '">' + hgrp + subG + ' --- ' + arr[1] + '</option>');
                    });
                });
            });
        } else {
            $('#chrName').html('');
            $('#submit').hide();
        }
    });

    $('#start, #end').change(function () {
        fragLen();
    });

    function fragLen() {
        var s = parseInt($('#start').val()), e = parseInt($('#end').val());
        if (isNaN(s) || isNaN(e)) {
            return $('#frag-len').html('');
        }
        return $('#frag-len').html(e - s + 1);
    }

    $('#submit').click(function () {
        $('#select-seqs').show();
        if ($('#singleQ').is(':checked')) {
            show_single_seq();
        } else if ($('#multipleQ').is(':checked')) {
            show_multiple_seqs();
        }
    });

    $('#next-n').click(function () {
        var end = parseInt($('#end').val()), n = parseInt($('#each-step').val());
        var start = end + 1;
        end += n;
        $('#start').val(start);
        $('#end').val(end);
        fragLen();
        show_single_seq();
    });

    $('#previous-n').click(function () {
        var start = parseInt($('#start').val()), n = parseInt($('#each-step').val());
        start = start - n;
        start = (start > 0) ? start : 1;
        var end = start + n - 1;
        $('#start').val(start);
        $('#end').val(end);
        fragLen();
        show_single_seq();
    });

    function show_single_seq() {
        $('#seq').html('');
        var start = parseInt($('#start').val()), end = parseInt($('#end').val());
        if (isNaN(start) || isNaN(end)) {
            $('#seq').text('start or end is not an integer!');
            return;
        }
        if (start <= 0) {
            $('#seq').text('Start should greater than or equal to 1!');
            return;
        }
        if (start > end) {
            $('#seq').text('Start should less than or equal to end!');
            return;
        }
        var strand = $('#revcom').is(':checked') ? -1 : 1;
        get_a_seq(start, end, strand);
        $('#next-previous').show();
    }

    $('.help').hide();
    $('.multiple').hide();

    $('#singleQ').click(function () {
        $('.single').show();
        var start = parseInt($('#start').val()), end = parseInt($('#end').val());
        if (isNaN(start) || isNaN(end)) {
            $('#next-previous').hide();
        }
        $('.multiple').hide();
    });
    $('#multipleQ').click(function () {
        $('.single').hide();
        $('.multiple').show();
    });

    $('#show-col-help').click(function (event) {
        event.preventDefault();
        $('#col-help').toggle();
    });
    $('#coords-example').click(function (event) {
        event.preventDefault();
        $('#coords').val("1\t50\t+\n51\t100\t+\n101\t200\t-");
    });

    $('#select-seqs').click(function (event) {
        event.preventDefault();
        SelectText('seq');
    });

    $('#colorful').change(function(){
        if ($(this).is(':checked')) {
            $('.for-colorful').show();
        } else {
            $('.for-colorful').hide();
        }
    })

    // 说明有多少行数据无效
    function show_multiple_seqs() {
        reset_nValid_nInvalid();

        $('#seq').html('');
        var coords = $('#coords').val();
        $.trim(coords);
        if (!coords) { // no content
            return $('#seq').html('Input nothing!');;
        }

        var data = coords.split('\n');
        var nTop = parseInt($('#nTop').val());
        data = data.length > nTop ? data.slice(0, nTop) : data;
        $.each(data, function (i, d) {
            var sEs = d.split(/\s+/);
            var start = parseInt(sEs[0]),
                end = parseInt(sEs[1]),
                strand = 1;
            if (sEs[2] && sEs[2] === '-') strand = -1;
            if (start > 0 && end > start) {
                get_a_seq(start, end, strand);
                addnValid();
            } else {
                addnInvalid();
            }
        });

    }

    // 目前 fasta 文件里核苷酸没有奇怪字符，暂时只有 ATCGN
    // 如果有，这里就会乱起 class 名
    function colorful_seq(json, start, end, strand, nchar) {
        var lines = json.seq.split('\n');
        $('#seq').append('<span class="fa_header">' + lines.shift() + '</span>' + "\n");
        var n = 0;
        $.each(lines, function (idx, seg) {
            if (seg.length === 0) return;

            var chars = seg.split('');
            $.each(chars, function (i, nc) {
                n += 1;
                if (strand === 1) {
                    $('#seq').append('<span class="dna-' + nc + '" title="' + n + ' - ' + (start + n - 1) + '">' + nc + '</span>');
                } else {
                    $('#seq').append('<span class="dna-' + nc + '" title="' + n + ' - ' + (end - n + 1) + '">' + nc + '</span>');
                }

                if (n % nchar === 0) {
                    $('#seq').append("\n");
                }
            });
        });
        // 避免 \n 重复
        if (n % nchar != 0) $('#seq').append('\n');
    }

    function plain_seq(json) {
        $('#seq').append(json.seq);
    }

    function get_a_seq(start, end, strand) {
        var nchar = parseInt($('#nchar-L').val());
        nchar = (nchar >= 1) ? nchar : 60;

        $.getJSON('/faidx/handle_ajax', {
            species: $('#species').val(),
            chrName: $('#chrName').val(),
            start: start, end: end, strand: strand
        }, function (json) {
            if ($.isEmptyObject(json) || $.isEmptyObject(json.seq)) {
                $('#seq').text('Got nothing!');
                return;
            }
            if ($('#colorful').is(':checked')) {
                colorful_seq(json, start, end, strand, nchar);
            } else {
                plain_seq(json);
            }
        });
    }

    function reset_nValid_nInvalid() {
        $('#nValid').text(0);
        $('#nInvalid').text(0);
    }

    function addnValid() {
        var n = parseInt($('#nValid').text());
        $('#nValid').text(n + 1);
    }

    function addnInvalid() {
        var n = parseInt($('#nInvalid').text());
        $('#nInvalid').text(n + 1);
    }
});

// 选中文字
// from: https://stackoverflow.com/questions/985272/selecting-text-in-an-element-akin-to-highlighting-with-your-mouse
function SelectText(id) {
    var doc = document
        , text = doc.getElementById(id)
        , range, selection
        ;
    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}