$(document).ready(function () {
    $(window).scroll(function () {
        var st = $(document).scrollTop();
        (st > 0) ? $('.back-top').show() : $('.back-top').hide();
    });
    $('.back-top a').click(function (event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, 120);
    });
});