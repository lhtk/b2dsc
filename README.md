## 目录
* [部署实例](#部署实例)
* [项目介绍](#项目介绍)
* [安装](#安装)
* [使用说明](#使用说明)
* [依赖](#依赖)
* [配置](#配置)
    - [自动化添加数据库](#自动化添加数据库)
    - [B2DSC](#b2dsc)
        + [基础配置](#基础配置)
        + [Known probes 配置](#known-probes-配置)
        + [species](#species)
            * [获取未知碱基的分布数据](#获取未知碱基的分布数据)
    - [Faidx](#faidx)
* [安全](#安全)
* [版权](#版权)
* [协议](#协议)
* [引用](#引用)

## 部署实例
* <http://mcgb.uestc.edu.cn/b2dsc>
* <http://main.waooat.cn/b2dsc>

## 项目介绍
B2DSC (Get the distribution of sequences on chromosomes, based upon BLASTN) 是基于 Perl real-time web framework [Mojolicious][1] 的一个应用。
主要是为了用图片的形式展示序列（特别是重复序列）在染色体上的分布情况。

例如 pSc119.2-1 (CCGTTTTGTGGACTATTACTCACCGCTTTGGGGTCCCATAGCTAT) 在小麦 21 条染色体上的大致分布：

<img src="./public/img/help/b2dsc/pSc119.2-1.png" alt="example" height="500">

目前只支持 **Linux**。

## 安装

```
# 直接克隆
git clone https://gitee.com/lhtk/b2dsc.git
```

或者下载 zip 包后解压。

## 使用说明

这是基于 [Mojolicious][1] 的一个简单应用，所以启用方法如任何一个基于它的 App。

启用。

```Shell
# 示例
# 在 b2dsc 主目录下

# 开发、调试
morbo -l "http://*:8080" -w ./ script/b2dsc

# 或在生产环境下部署
hypnotoad script/b2dsc
# 终止运行，可使用
hypnotoad -s script/b2dsc
```

而后可以在浏览器访问：<http://localhost:8080/>

更多启动方法可以参考 [Mojolicious 的文档](http://mojolicious.org/perldoc/Mojolicious/Guides/Cookbook#DEPLOYMENT)。

数据库的配置参考: [配置](#配置)。

## 依赖
### 可能需要安装的
* [NCBI-BLAST+ suite](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download)
    * Linux 发行版可直接在软件仓库找 `ncbi-blast+`
    * 版本要求 v2.2.30+
* [Samtools](https://www.htslib.org/)
* [bgzip](https://www.htslib.org/doc/bgzip.html)
* [BioPerl](http://bioperl.org/) 中的模块
    * [Bio::SeqIO](https://metacpan.org/pod/Bio::SeqIO)
* [Mojolicious][1] web 框架
    * 作者机子上版本为 v6.15
* 其他可能需要安装的 Perl 模块
    * [JSON](https://metacpan.org/pod/JSON)

### 用到的 JavaScript 库
* [jQuery](http://jquery.com/): A fast, small, and feature-rich JavaScript library (v3.1.1).
* [D3](https://d3js.org/): (or D3.js) is a JavaScript library for visualizing data using web standards. (v4).
* [Spectrum.js](https://bgrins.github.com/spectrum): The No Hassle JavaScript Colorpicker (v1.8.0).
* [FileSaver](https://github.com/eligrey/FileSaver.js/)

## 配置
配置文件在 **conf** 目录下，目前包括 b2dsc 和 faidx 的配置，都是 JSON 格式。

第一次运行项目的时候，关键的配置文件会自动初始化，即由 `xxx.json.raw` 拷贝为 `xxx.json`。

```
.
├── conf
│   ├── b2dsc
│   │   └── species
│   └── faidx
|........
```

### 自动化添加数据库
可使用 [prepare_data4b2dsc.pl](./utils/prepare_data4b2dsc.pl) 来配置新数据库。
该程序额外依赖 [HTSlib](https://github.com/samtools/htslib) 中的 `bgzip` 命令。

```Shell
# 用法示例：
# 
# 若是未压缩的纯文本 FASTA 文件：
./prepare_data4b2dsc.pl -f IWGSC_RefSeq_V1_chr1A.fsa -n wheat_1A -r -t chrom-id-table.tab -b ~/git/b2dsc
#
# 注意：-r 是删除用户提供的原始 FASTA 文件，若不删除，则勿启用该选项。
# 
# 若是普通 gzip 文件：
./prepare_data4b2dsc.pl -f IWGSC_RefSeq_V1_chr1A.fsa.gz -z -n wheat_1A -r -t chrom-id-table.tab -b /home/lhtk/git/b2dsc
# 
# 若是 bgzip 构建的 gzip 文件：
./prepare_data4b2dsc.pl -f IWGSC_RefSeq_V1_chr1A.fsa.gz -bgz -n wheat_1A -t chrom-id-table.tab -b /home/lhtk/sites/mcg

# 具体请查看该程序的帮助
./prepare_data4b2dsc.pl -h
```

### B2DSC
#### 基础配置
基础配置文件为 `conf/b2dsc/b2dsc.json`。

内含项：

* `days_kept` 临时文件保存天数。
    - 当前值: 7
* `data_dir`: 临时文件目录，不提供 web 访问。
* `public_tmp_dir`: 临时文件目录，可 web 访问。
* `min_seq_length`: blastn 比对接受的最小序列长度。
    - 当前值: 10
* `max_num_of_seq`: 单个任务最多接受多少条有效序列。
    - 当前值: 20
    - 设置为 0 时不限制序列数目。
* `threads`: 单次 blastn 线程数。
    - 当前值: 2
    - 因为我的机器很差
* `blastn_short`: 对长度多少多少以下（&le;）的序列采用 `-task blastn-short`。
    - 当前值: 30
* `ofmt`: blastn 输出格式，目前只接受格式 6，即 Tabular 格式。
    - 至少为: "6 sseqid pident qcovhsp sstrand sstart send"，缺少其中一个 specifier 则要出错。
* `ofmt_specifiers`: 供 B2DSC 服务主页面调用，一般不要改。

#### Known probes 配置
配置文件：`conf/b2dsc/known_probes.json`。

很简单，JSON Object 格式。

```
# 这是说明
# "给数据库起的名字": "job id"

{
    "example1": "k4example1",
    "example2": "k4example2",
    "example3": "k4example3"
}

# 这是说明
# k4example: keep for 'example'
```

将适用于某物种的已知的探针序列用 B2DSC 分析，得到结果后，将原先的以 t 开头的 `job_id` 更改为以 k 开头（或其他非 t 字符），从而不被自动删除。

需要把相关 id 都改掉，包括临时任务文件夹名字、`done.json` 内含的。

例如：

1. 原 `job_id`，比如 `t58ef2f09` 变更为 `k58ef2f09`，`data_dir` 和 `public_tmp_dir` 目录下对应文件夹名字都要修改。
2. `public_tmp_dir` 目录下，需要把**例如** `k58ef2f09` 文件夹里的 `done.json` 的 `t58ef2f09` 都替换为 `k58ef2f09`。

#### species
参考基因组“数据库”配置。
需要放在 `conf/b2dsc/species` 目录下。

每个数据库一个配置文件，比如 `example.json` 是 example 数据库的配置文件。

**注意**：当前，数据库名必须只含 words，也就是: 英文字母 `A-Za-z` + 数字 `0-9` + 下划线 `_`。

配置举例说明：

```
# 实际配置文件中不可以添加注释！
{
    // blast database，必需配置
    "blastdb": {
        "db_dir": "data/b2dsc/blastdb/example", // 所在目录
        // 需要给每条染色体建一个 blast database
        "chromosome": {
            // 每个亚基因组一个散列，即使物种没有什么亚基因组说法，也要虚构一个。
            // 比如这里虚构了个 A 亚基因组。
            "A": {
                // 1A 染色体
                "1": [
                    "1A", // blast database 的名字
                    "chr1A" // 构建该 blast database 的染色体序列的名字
                ],
                "2": [
                    "2A",
                    "chr2A"
                ]
            },
            "B": {
                "1": [
                    "1B",
                    "chr1B"
                ],
                "2": [
                    "2B",
                    "chr2B"
                ]
            }
        }
    },
    // 必需配置
    "chromosome_length": {
        // 同样，每个“亚基因组”一个数组。
        "A": [
            10000000, // 1A 染色体长度
            12345678 // 2A 的
        ],
        "B": [
            17638475,
            18234567
        ]
    },
    // 染色体中未知碱基 N 的分布数据，必需配置
    // unknown or unspecified
    "N": {
        "dir": "data/b2dsc/Ns/example", // 目录
        "chromosome": {
            // 同样，每个“亚基因组”一个散列。
            "A": {
                "1": "1A.tab", // 存储了 1A 数据的文件
                "2": "2A.tab"
            },
            "B": {
                "1": "1B.tab",
                "2": "2B.tab"
            }
        }
    },
    // 可选配置
    // 参考序列，比如着丝粒特异重复序列
    // 这里以植物端粒富集的简单重复序列为例
    "refSeq": {
        "id": "telomeric", // 序列 id
        "desc": "guanosine-rich (T3AG3) telomeric DNA ", // 描述
        "seq": "TTTAGGGTTTAGGGTTTAGGGTTTAGGGTTTAGGGTTTAGGGTTTAGGGTTTAGGG", // 序列
        "jid": "k4example", // 任务 id，不可以 t 开头。
        "pident": 85, // 对参考序列分析时使用的过滤参数值
        "qcovhsp": 80 // 对参考序列分析时使用的过滤参数值
    }
}
```

可以参考 [example.json.raw](./conf/b2dsc/species/example.json.raw) 和 [wheat.json.example](conf/b2dsc/species/wheat.json.example)。

总的来说，有 4 块：

1. `blastdb`: 必需项
2. `chromosome_length`: 必需项
3. `Ns`: 必需项
4. `refSeq`: 可选项

##### 获取未知碱基的分布数据
未知碱基（unknown or unspecified nucleotides，在 FASTA 格式中一般以 N 表示）。

可以用 `utils/find-N.pl` 来获取染色体序列中 N 的分布。

```Shell
# 不加参数会打印使用帮助
path/to/find-N.pl
# 这里 path/to 的意思是到 find-N.pl 的路径

# 使用示例
path/to/find-N.pl -i chr1A.fa -o 1A.tab
# 假设 chr1A.fa 是小麦 1A 染色体的组装序列。
```

`utils/find-N.pl` 的输出像这样：

```
2177	3362
17309	18396
26330	26802
34715	35573
43285	44122
52064	52817
```

只有两列数据，记录每一段 N 的起止。

### Faidx
每个参考基因组一个配置文件，放置于 `conf/faidx` 目录下。

配置举例说明：

```
# 实际配置文件中不可以添加注释！
{
    "dir": "data/b2dsc/fasta/example", // FASTA 文件目录
    "chromosome": {
        // 同样，每个“亚基因组”一个散列。
        "A": {
            "1": [
                "chr1A.fa", // FASTA 序列文件，当然也可以是 bgzip 构建的压缩文件。
                "chr1A", // 序列 id
                10000000 // 序列长度
            ],
            "2": [
                "chr2A.fa",
                "chr2A",
                12345678
            ]
        },
        "B": {
            "1": [
                "chr1B.fa",
                "chr1B",
                17638475
            ],
            "2": [
                "chr2B.fa",
                "chr2B",
                18234567
            ]
        }
    }
}
```

可以参考 [example.json.raw](conf/faidx/example.json.raw) 和 [wheat.json.example](conf/faidx/wheat.json.example)。

## 安全
因为作者是个野生的半瓶水程序员，可能会存在安全问题。

## 版权
&copy; 2019 [lhtk](mailto:lhtk80t7@qq.com)

## 协议
Artistic License 2.0。

## 引用
如果您在工作中使用 B2DSC，请引用：

* Tao Lang, Guangrong Li, Hongjin Wang, Zhihui Yu, Qiheng Chen, Ennian Yang, Shulan Fu, Zongxiang Tang, Zujun Yang. Physical location of tandem repeats in the wheat genome and application for chromosome identification\[J\]. Planta, 2019, 249(3): 663-675. DOI:[10.1007/s00425-018-3033-4][2]

和/或

* 郎涛. 小麦及其近缘物种串联重复序列的全基因组发掘与染色体区段鉴定\[D\]. 电子科技大学, 2019, 14-34

[1]: http://www.mojolicious.org/
[2]: https://link.springer.com/article/10.1007%2Fs00425-018-3033-4